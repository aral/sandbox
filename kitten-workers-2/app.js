import http from 'node:http'

http.createServer(function (request, response) {
  response.end(
    `<html>
      <head>
        <title>Refresh test</title>
        <meta http-equiv="refresh" content="0.5;url=/" />
      </head>
      <body>
        <h1>This is a test</h1>
        <p>Wooh baby!</p>
      </body>
    </html>`)
}).listen(8000, () => {
  console.info('Listening on http://localhost:8000')
})
