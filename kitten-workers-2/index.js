import cluster from 'node:cluster'

if (cluster.isPrimary) {
  // We are the primary process (the controller).
  
  let serverProcess = null // The main server worker.

  function createNewServerProcess () {
    serverProcess = cluster.fork()

    serverProcess.on('exit', _ => {
      console.info('Old server exited, creating new one…')
      createNewServerProcess()
    })

    serverProcess.on('listening', async _ => {
      console.info('New server process (worker) is listening.')
    })
  }
  
  // Create the first server.
  createNewServerProcess()
  
  // Let’s kill the service every second.
  setInterval(() => {
    if (serverProcess) {
      serverProcess.kill('SIGTERM')
    } else {
      console.info('Server process doesn’t exist. Not killing.')
    } 
  }, 1000)
} else {
  // This is not the primary worker; just load the server.
  await import('./app.js')
}
