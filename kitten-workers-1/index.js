import cluster from 'node:cluster'

if (cluster.isPrimary) {
  // We are the primary process (the controller).
  
  let mainWorker      = null // The main server worker.
  let temporaryWorker = null // Used to spin up a new instance of the server.
  let switchingWorkers = false

  function switchWorkers () {
    console.info('Switching workers…')
    mainWorker = temporaryWorker
    temporaryWorker = null
  }
  
  function safelyCreateNewServer () {
    temporaryWorker = cluster.fork()

    temporaryWorker.on('exit', _ => {
      console.info('Worker has exited.')
      if (!switchingWorkers) {
        // The worker process has exited but it’s not
        // because we’re switching workers. So start a
        // new instance of it to keep the server going.
        // TODO: Should we run two so there’s no
        // downtime should one fail?
        console.warn('Worker has unexpectedly disconnected. Restarting it.')
        temporaryWorker = cluster.fork()
      } else {
        // We’re switching workers. Since this worker is now offline,
        // we can switch the temporary and main workers.
        switchWorkers()
        switchingWorkers=false
      }
    })

    temporaryWorker.on('listening', async _ => {
      console.info('New server (worker) is online.')
      if (mainWorker === null) {
        // This is the first time we’re loading a worker.
        switchWorkers()
      } else {
        // A main worker already exists. Terminate it so
        // we only have one instance of the server running.
        // Note: running terminate, not disconnect here
        // so that it happens as quickly as possible. Since
        // this will be used during production, it should
        // not be an issue.
        console.info('About to terminate the existing worker.')
        switchingWorkers = true
        mainWorker.kill('SIGTERM')
      }
    })
  }
  
  // See if any worker processes exist and create one if they do not.
  const workers = Object.keys(cluster.workers)

  if (workers.length === 0) {
    safelyCreateNewServer()
  }

  // Every three seconds, let’s start a new temporary worker,
  // kill the main worker, and switch the termporary worker over
  // to the main.
  setInterval(() => {
    if (!switchingWorkers) {
      safelyCreateNewServer()
    } else {
      console.log('Switching workers: ignoring interval-based worker refresh request.')
    }
  }, 1000)
} else {
  // This is not the primary worker; just load the server.
  await import('./app.js')
}
