// Emoji map used to encode a person’s ed25519
// private key. Emoji is chosen on purpose to
// make it impossible for the person to type it in
// thereby necessitating the use of copy and paste
// and a password manager (and hence good security
// hygiene).
//
// The emoji chosen are mainly from the animals and plants
// and food groups with some exceptions.
//
// All emoji in this list are surrogate pairs without any
// special modifiers.

import assert from 'node:assert/strict'

const emoji = [
  "🐵",
  "🐒",
  "🦍",
  "🦧",
  "🐶",
  "🐕",
  "🦮",
  "🐩",
  "🐺",
  "🦊",
  "🦝",
  "🐱",
  "🐈",
  "🦁",
  "🐯",
  "🐅",
  "🐆",
  "🐴",
  "🧮",
  "🦄",
  "🦓",
  "🦌",
  "🦬",
  "🐮",
  "🐂",
  "🐃",
  "🐄",
  "🐷",
  "🐖",
  "🐗",
  "🐽",
  "🐏",
  "🐑",
  "🐐",
  "🐪",
  "🐫",
  "🦙",
  "🦒",
  "🐘",
  "🦣",
  "🦏",
  "🦛",
  "🐭",
  "🐁",
  "🐀",
  "🐹",
  "🐰",
  "🐇",
  "🎈",
  "🦫",
  "🦔",
  "🦇",
  "🐻",
  "🐨",
  "🐼",
  "🦥",
  "🦦",
  "🦨",
  "🦘",
  "🦡",
  "🐾",
  "🦃",
  "🎹",
  "🐓",
  "🐣",
  "🐤",
  "🐥",
  "🐦",
  "🐧",
  "💕",
  "🦅",
  "🦆",
  "🦢",
  "🦉",
  "🦤",
  "🪶",
  "🦩",
  "🦚",
  "🦜",
  "🚲",
  "🐊",
  "🐢",
  "🦎",
  "📚",
  "🐉",
  "🦕",
  "🦖",
  "🐳",
  "🐋",
  "🐬",
  "🦭",
  "🐟",
  "🐠",
  "🐡",
  "🦈",
  "🐙",
  "🐚",
  "🐌",
  "🦋",
  "🐛",
  "🐜",
  "🐝",
  "🪲",
  "🐞",
  "🦗",
  "🎭",
  "🎁",
  "🧬",
  "🪱",
  "🦠",
  "💐",
  "🌸",
  "🎠",
  "🌈",
  "🌹",
  "🧣",
  "🌺",
  "🌻",
  "🌼",
  "🌷",
  "🌱",
  "🪴",
  "🌲",
  "🌳",
  "🌴",
  "🌵",
  "🌾",
  "🌿",
  "🎤",
  "🍀",
  "🍁",
  "🪺",
  "👽",
  "🍇",
  "🍈",
  "🍉",
  "🍊",
  "🍋",
  "🍌",
  "🍍",
  "🥭",
  "🍎",
  "🍏",
  "🍐",
  "🍑",
  "🍒",
  "🍓",
  "🫐",
  "🥝",
  "🍅",
  "🫒",
  "🥥",
  "🥑",
  "🍆",
  "🥔",
  "🥕",
  "🌽",
  "🧸",
  "🫑",
  "🥒",
  "🥬",
  "🥦",
  "🧄",
  "🧅",
  "🍄",
  "🥜",
  "🌰",
  "🍞",
  "🥐",
  "🥖",
  "💩",
  "🥨",
  "🥯",
  "🥞",
  "🧇",
  "🧀",
  "🎶",
  "🏸",
  "🎾",
  "🎨",
  "🍔",
  "🔭",
  "🍕",
  "🌭",
  "🥪",
  "🌮",
  "🌯",
  "😸",
  "📷",
  "🌜",
  "🥚",
  "🚂",
  "🛼",
  "🚁",
  "👾",
  "👻",
  "🥗",
  "🍿",
  "🧩",
  "🖖",
  "🥫",
  "🎸",
  "🍘",
  "🍙",
  "🍚",
  "🃏",
  "🍜",
  "🍝",
  "🍠",
  "🍢",
  "🍣",
  "🍤",
  "🍥",
  "🥮",
  "🍡",
  "🥟",
  "🥠",
  "🩰",
  "🦀",
  "🦞",
  "🦐",
  "🦑",
  "🎡",
  "🍦",
  "🍧",
  "🍨",
  "🍩",
  "🍪",
  "🎂",
  "🍰",
  "🧁",
  "🥧",
  "🍫",
  "🍬",
  "🍭",
  "🍮",
  "🎓",
  "🍼",
  "🎮",
  "🛹",
  "🫖",
  "🌍",
  "🌎",
  "🌏",
  "🧭",
  "🌠",
  "🪐",
  "🪀",
  "🧵",
  "🧶",
  "🧋",
  "🎉",
  "🪁",
  "🙈",
  "🙉",
  "🙊",
]

//
// Methods
//

const emojiIndex = emoji.reduce((object, character, index) => {
  object[character] = index
  return object
}, {})

function secretToEmojiString (secret) {
  return secret.reduce((emojiString, currentByteValue) => emojiString + emoji[currentByteValue], '')
}

function emojiStringToSecret (emojiString) {
  const emojiSegments = Array.from(new Intl.Segmenter().segment(emojiString)).map(object => object.segment)
  return Uint8Array.from(emojiSegments.map(character => emojiIndex[character]))
}

//
// Assertions
//

assert.strictEqual(emoji.length, 256)
 
// Ensure no duplicates.
emoji.forEach((e, i) => {
  emoji.forEach((e2, i2) => {
    if (i != i2) {
      assert.notStrictEqual(e, e2, `${e} (index ${i}) should not be duplicated at index ${i2}`)
    }
  })
})

// Test emoji lenghts.

emoji.forEach((e, i) => { 
  const codePoint1 = e.codePointAt(0)
  const codePoint2 = e.codePointAt(1)
  const codePoint3 = e.codePointAt(2)

  const recreated = String.fromCodePoint(codePoint1)
  const canBeRecreatedFromCodePoint1 = (e == recreated)

  assert.notStrictEqual(codePoint1, undefined)
  assert.notStrictEqual(codePoint2, undefined)
  assert.strictEqual(codePoint3, undefined)
  assert.strictEqual(canBeRecreatedFromCodePoint1, true)
})

// Test methods.

const secretWithAllPossibleByteValues = Uint8Array.from([...Array(256).keys()])
const emojiString = secretToEmojiString(secretWithAllPossibleByteValues)
const secretFromEmojiString = emojiStringToSecret(emojiString)

const secretsAreEqual = secretWithAllPossibleByteValues.reduce((current, previous, index) => current && (previous === secretFromEmojiString[index]), true)
assert.strictEqual(secretsAreEqual, true)

console.log(emojiString)
console.log('\nAll assertions passed.')
