#!/usr/bin/env bash

esbuild index.js --bundle --format=esm --minify --outfile=build/index.js
