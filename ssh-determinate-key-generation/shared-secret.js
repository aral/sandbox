import * as ed from '@noble/ed25519'
import * as aes from 'micro-aes-gcm'

// Alice
const alicePrivate = ed.utils.randomPrivateKey()

console.log('Alice’s secret', alicePrivate)
console.log('Alice’s secret (in HEX)', ed.utils.bytesToHex(alicePrivate))
console.log('Alice’s secret (in HEX via BUFFER)', Buffer.from(alicePrivate).toString('hex'))
const alicePublic = await ed.getPublicKey(alicePrivate)

// console.log(alicePublic.reduce((previous, current) => previous + )
console.log('Alices ID', alicePublic)
console.log('Alice’s ID (in HEX)', ed.utils.bytesToHex(alicePublic))

console.log('Alice’s ID from HEX', ed.utils.hexToBytes(ed.utils.bytesToHex(alicePublic)))
// Bob
const bobPrivate = ed.utils.randomPrivateKey()
const bobPublic = await ed.getPublicKey(bobPrivate)

const sharedKeyA = await ed.getSharedSecret(alicePrivate, bobPublic)
const sharedKeyB = await ed.getSharedSecret(bobPrivate, alicePublic)

console.log(sharedKeyA)
console.log(sharedKeyB)

const message = 'Hello, world!'

const cipherText = await aes.encrypt(sharedKeyA, message)
const iv = cipherText.slice(0, 12)
const mac = cipherText.slice(-16)

console.log('cipherText', cipherText)
console.log('iv', iv)
console.log('mac', mac)

const plainText = aes.utils.bytesToUtf8(await aes.decrypt(sharedKeyB, cipherText))

console.log('Decrypted text', plainText)

