//////////////////////////////////////////////////////////////////////
//
// Kitten Crypto API
//
// Exposes high-level cryptographic functions to enable generation
// of secret key material in the person’s own browser without ever
// hitting the server and, from there, generating the ed25519 and
// ssh public keys that are sent to the server for storage.
//
//////////////////////////////////////////////////////////////////////

import ssh from 'ed25519-keygen/ssh'
import pgp from 'ed25519-keygen/pgp'
import * as ed from '@noble/ed25519'
import * as aes from 'micro-aes-gcm'

// Note: randomBytes from Noble is polymorphic and bails if
// cryptographically-secure random number generator is not
// present in the environment (Node/Browser/Deno/fridge, etc.)
import { randomBytes } from 'ed25519-keygen/utils'

// const secretSeed = randomBytes(32)
const secretSeed = ed.utils.randomPrivateKey()
// console.log(Buffer.from(secretSeed).toString('hex'))
console.log('secretSeed', secretSeed)

const secretKeys = await ssh(secretSeed, 'place.small-web.org')
console.log(secretKeys.fingerprint)
console.log(secretKeys.privateKey)
console.log(secretKeys.publicKey)

export async function createKeys(domain) {
  const _keys = {
    ed25519: {
      private: ed.utils.randomPrivateKey()
    }
  }
  const keys = Object.assign(_keys, deriveKeysFromSecretKeyForDomain(_keys.ed25519.private, domain))
  console.log(keys)
  return keys
}

export async function deriveKeysFromSecretKeyForDomain(secretKey, domain) {
  const ed25519PublicKey = await ed.getPublicKey(secretKey)
  const sshKeys = await ssh(secretKey, /* comment */ domain)
  const pgpKeys = await pgp(secretKey, /* comment */ domain)
  return {
    ed25519: {
      public: ed25519PublicKey
    },
    ssh: sshKeys,
    pgp: pgpKeys
  }
}

